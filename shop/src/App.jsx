import { Routes, Route } from "react-router-dom";
import { Home, Fav, Basket } from './pages';
import Header from './components/Header';
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { getProductsFetch } from "./redux/actions/products";
import { useSelector } from "react-redux";

function App() {
  const dispatch = useDispatch();
  const products = useSelector((state) => state.products.products);

  useEffect(() => {
    dispatch(getProductsFetch("./goods.json"));
  }, [dispatch]);

  if (!products.length) {
    return <h1>Loading...</h1>
  }
  return (
    <>
    <Header/>
    <Routes>
      <Route 
        path='/' 
        element={<Home/>}
      />
      <Route 
        path='/fav' 
        element={<Fav/>}
      />
      <Route 
        path='/basket' 
        element={<Basket/>}
      />
    </Routes>
    </>
  );
}

export default App;